$(function(){
    var obj = {
        init : function(){
            this.setElements();
            this.setEvent();
        },
        setElements : function(){
            this.exam = $('.exam_wrap');
            this.button =  this.exam.find('button');
            this.range = [[8,8] , [2,9]];
            this.array = [];
        },
        setEvent : function(){
            this.button.on('click',  $.proxy(this.multiplication, this));
        },
        multiplication : function(e){
            this.array = [];
            var range = this.range[$(e.target).parent().index()];

            for(var num = range[0]; num <= range[1]; num++ ){
                //var temp = [];
                this.array[num-range[0]]= [];

                for(var cnt = 1; cnt < 10; cnt++){
                    //temp.push(num * cnt);
                    this.array[num-range[0]][cnt-1]=num * cnt;
                }
                //this.array.push(temp);
            }

            $(e.target).siblings('p')[0].innerHTML  = this.array;
            this.printArry();
        },
        printArry : function(){
            console.log('-------------------------');
            for(var i=0; i < this.array.length; i++){
                console.log(this.array[i].join());
            }
            console.log('-------------------------');
        }
    };
    obj.init();
});