$(function(){
	var obj = {
		init : function(){
			this.setElements();
			this.setEvent();
		},
		setElements : function(){
			// 선택 곰
			this.bxWrap = $('.bxlst');
			this.bxList = this.bxWrap.children();

			// 뛰는 곰
			this.gomWrap = $('.game_area');
			this.gomList = this.gomWrap.children();

			// '선택하기' 버튼		
			this.btn = $('.btn');

			
			// 초기화
			this.initElements();
		},
		initElements : function(){
			// 당첨 구분
			this.succese = true;

			this.bxList .find('input').prop('checked', false);
			this.gomList.removeClass('winlane')
			this.gomList.find('div').removeAttr('style');
			this.checkIndex;
			this.eventCall = true;

		},
		setEvent : function(){
			this.bxList.on('change', 'input', $.proxy(this.change, this));
			this.btn.on('click',  $.proxy(this.racing, this)); 
		},
		change: function(e){
			_this = $(e.currentTarget);
			this.checkIndex = _this.parent('li').index();

			// 1등 고르기 
			if(!this.succese) this.setWinner(); 
		},
		racing : function(e){
			if(this.bxList.find('input:checked').length <= 0 || !this.eventCall){
				if(!this.eventCall){ 
					alert('중복 참여 불가');
				}else{
					alert('체크 해');
				}
				return e.preventDefault();
			}
			this.eventCall = false;	

			for(var i =0; i  < this.gomList.length ; i++){
				var _this = this.gomList[i];
				var speed = (i == this.checkIndex) ? 800 : 1000;
								
				if( i == this.checkIndex ){
					$(_this).find('.gom').animate({ top:"98px"},speed, function(){
						$(this).parent().addClass('winlane');
					});
				} else{
					$(_this).find('.gom').animate({ top:"98px"},speed);		
				}
			};
		},
		setWinner: function(){
			var array = [];
			var _index = this.checkIndex;
			this.bxList.each(function(i){
				array.push( i );
			});
			while(_index == this.checkIndex){
				this.checkIndex =  Math.floor(Math.random() * array.length);
			}
		}
	};
	obj.init();
});