$(function(){
	var exam1 = $('.exam_q').eq(0),
		exam2 = $('.exam_q').eq(1),
		exam3 = $('.exam_q').eq(2),	
		exam4 = $('.exam_q').eq(3);		

	var _input1 = function(){
		var a = 0;
		while(a < 5){
			exam1.append('<div><input type="checkbox">' + (a + 1) + '</div>');
			a++;
		};
	};
	_input1();

	var _input2 = function(){
		for(i = 0; i < 5; i++){
			if(i % 2 === 0){
				exam2.append('<div><input type="text" placeholder="홀수"></div>');
			} else {
				exam2.append('<div><input type="checkbox">짝수</div>');
			}
		};
	};
	_input2();

	var _table1 = function(){
		for(i = 1; i < 10; i++){
			exam3.append("<div>" + "8" + " x " + i + " = " + 8 * i + "</div>");
		};
	};
	_table1();

	var _table2 = function(){
		var a = 2;
		while(a < 10){
			for(i = 1; i < 10; i++){
				exam4.append("<div>" + a + " x " + i + " = " + a * i + "</div>");
			}
			a++;
		}
	}
	_table2();
});