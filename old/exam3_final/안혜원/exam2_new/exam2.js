$(function(){
	var exam1 = $('.exam_wrap').eq(0),
	exam1Btn = exam1.find('button'),
	exam1List = exam1.find('ul'),
	exam1ListChild = exam1List.find('li'),
	exam2 = $('.exam_wrap').eq(1),
	exam2Btn = exam2.find('button'),
	exam2List = exam2.find('ul'),
	exam2ListChild = exam2List.find('li'),
	exam3 = $('.exam_wrap').eq(2),
	exam3Btn = exam3.find('button'),
	exam3List = exam3.find('ul'),
	exam3ListChild = exam3List.find('li'),
	exam3Minus = exam3.find('button').eq(0),
	exam3Plus = exam3.find('button').eq(1),
	_color1 = 'yellow',
	_color2 = 'yellow',
	_color3 = 'yellow',
	_index1 = 0,
	_index2 = exam2ListChild.length - 1,
	_index3Old = _index3 = 0;

	//exam1
	var _list1 = function(){
		exam1ListChild.eq(_index1).css('background', _color1);
		_index1++;
		if(_index1 >= exam1ListChild.length){
			_index1 = 0;
			_color1 = 'red';
		}
	};
	exam1Btn.on('click', _list1);
	//exam1

	//exam2
	var _list2 = function(){
		exam2ListChild.eq(_index2).css('background', _color2);
		_index2--;
		if(_index2 < 0){
			_index2 = exam2ListChild.length - 1;
			_color2 = 'red'
		}
	}
	exam2Btn.on('click', _list2);
	//exam2

	//exam3
	var _btnPlus = function(){
		_index3++;
		exam3ListChild.eq(_index3 - 1).animate({
			top: '0'
		})
		if(_index3 > exam3ListChild.length - 1) {
			_index3 = 0;
		}
		_reset();
	}

	var _btnMinus = function(){
		_index3--;
		exam3ListChild.eq(_index3 + 1).animate({
			top: '0'
		})
		if(_index3 < 0) {
			_index3 = exam3ListChild.length - 1;
		}
		_reset();
	}

	var _reset = function(){
		_color = 'yellow';
		exam3ListChild.css('background', '');
		exam3ListChild.eq(_index3).css('background', _color3).animate({
			top: '10px'
		});
		_index3Old = _index3;
	}
	exam3Plus.on('click', _btnPlus);
	exam3Minus.on('click', _btnMinus);
	//exam3
});