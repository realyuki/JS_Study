$(function(){

    // exam1 [s]
    var exam1 = $('.exam_wrap').eq(0),
    exam1Btn = exam1.find('button'),
    exam1List = exam1.find('ul'),
    exam1ListChild = exam1List.children(),
    exam1_color = 'yellow',
    exam1_index = 0;

    exam1Btn.on('click', function(){
        exam1ListChild.eq(exam1_index).css('background', exam1_color);
        exam1_index ++;
        if(exam1_index >= exam1ListChild.length){
            exam1_index = 0;
            exam1_color = 'red';
        }
    });
    // exam1 [e]

    // exam2 [s]
    var exam2 = $('.exam_wrap').eq(1),
    exam2Btn = exam2.find('button'),
    exam2List = exam2.find('ul'),
    exam2ListChild = exam2List.find('li'),
    exam2_color = 'yellow', 
    exam2_index = exam2ListChild.length - 1;

    exam2Btn.on('click', function(){
        exam2ListChild.eq(exam2_index).css('background', exam2_color);
        exam2_index --;
        if(exam2_index < 0){
            exam2_index = exam2ListChild.length - 1;
            exam2_color = 'red';
        }
    });
    // exam2 [e]

    // exam3 [s]
    var exam3 = $('.exam_wrap').eq(2),
    exam3Btn = exam3.find('button'),
    exam3List = exam3.find('ul'),
    exam3ListChild = exam3List.children(),
    exam3_color = 'yellow',
    exam3_index = exam3ListChild.length;

    exam3Btn.on('click', function(){
        var btnIndex = exam3Btn.index(this);
        if (btnIndex === 0) {
            exam3_index --;
            exam3ListChild.eq(exam3_index + 1).removeAttr('style');
            exam3ListChild.eq(exam3_index).css('background', exam3_color);
            if(exam3_index < 0) {
                exam3_index = exam3ListChild.length - 1;
            }
        } else if (btnIndex === 1) {
            if(exam3_index >= exam3ListChild.length) {
                exam3_index = -1;
            }
            exam3_index ++;
            exam3ListChild.eq(exam3_index - 1).removeAttr('style');
            exam3ListChild.eq(exam3_index).css('background', exam3_color);
        }  
    })
    // exam3 [e]

});