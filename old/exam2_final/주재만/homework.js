$(function() {
    'use strict';
    var q1Index = 0,
        q3Index = 0,
        q1BgIndex = 0,
        q3BgIndex = 0,
        bgColors = { '0': 'red', '1': 'orange', '2': 'yellow', '3': 'green', '4': 'blue', '5': 'darkblue', '6': 'violet' },
        bgColorsLength = Object.keys(bgColors).length,
        q2Index = bgColorsLength - 1,
        q2BgIndex = 0,
        autoStatus = false,
        event,
        intervalSecond = 0;

    $('button').on('click', function() {
        // 버튼 클릭 시 실행
        var parentObj = $(this).parent();
        var parentObjIndex = parentObj.index();
        var eventTargetList = parentObj.find('.exam_q > ul li');
        var eventTargetListLength = eventTargetList.length;

        if ($(this).hasClass('btn-warning')) {
            // 이벤트 초기화.
            $(".exam_q").eq(0).find('li').css('background', 'none');
            $(".exam_q").eq(1).find('li').css('background', 'none');
            $(".exam_q").eq(2).find('li').css('background', 'none');
            $(".exam_q").eq(2).find('li').eq(0).css('background', bgColors['2']);
            q1Index = 0;
            q2Index = bgColorsLength - 1;
            q3Index = 0;
            q1BgIndex = 0;
            q2BgIndex = 0;
            q3BgIndex = 0;
        } else if ($(this).hasClass('btn-success')) {
            // Auto Play 실행
            clearInterval(event);
            autoStatus = true;
            if (intervalSecond == 0) {
                // 클릭 후 delay없이 바로 이벤트 시작을 위해 추가됨.
                $('.exam_wrap').eq(0).find('button.auto-enable').trigger('click');
                $('.exam_wrap').eq(1).find('button.auto-enable').trigger('click');
                $('.exam_wrap').eq(2).find('button.auto-enable').trigger('click');
                intervalSecond = 1000;
            }

            if ($(this).hasClass('fast')) {
                // 2배속 Auto Play
                event = setInterval(function() {
                    $('.exam_wrap').eq(0).find('button.auto-enable').trigger('click');
                    $('.exam_wrap').eq(1).find('button.auto-enable').trigger('click');
                    $('.exam_wrap').eq(2).find('button.auto-enable').trigger('click');
                }, 500);
            } else {
                // 1초 간격 Auto Play
                event = setInterval(function() {
                    $('.exam_wrap').eq(0).find('button.auto-enable').trigger('click');
                    $('.exam_wrap').eq(1).find('button.auto-enable').trigger('click');
                    $('.exam_wrap').eq(2).find('button.auto-enable').trigger('click');
                }, 1000);
            }
        } else if ($(this).hasClass('btn-error')) {
            // Auto Play 중지
            autoStatus = false;
            clearInterval(event);
        } else {
            switch (parentObjIndex) {
                case 0:
                    // Qustion 1
                    eventTargetList.eq(q1Index++).css('background', bgColors[q1BgIndex]);
                    if (q1Index >= eventTargetListLength) {
                        q1Index = 0;
                        q1BgIndex = q1BgIndex + 1 >= bgColorsLength ? 0 : q1BgIndex + 1;
                    }
                    break;
                case 1:
                    // Qustion 2
                    if (q2Index < 0) {
                        q2Index = eventTargetListLength - 1;
                        q2BgIndex = q2BgIndex + 1 >= bgColorsLength ? 0 : q2BgIndex + 1;
                    }
                    eventTargetList.eq(q2Index--).css('background', bgColors[q2BgIndex]);
                    break;
                case 2:
                    // Qustion 3
                    if ($(this).hasClass('btn-info')) {
                        eventTargetList.eq(q3Index--).css('background', 'none');
                        if (q3Index < 0) {
                            q3Index = eventTargetListLength - 1;
                        }
                        eventTargetList.eq(q3Index).css('background', bgColors[q3BgIndex++]);
                    } else {
                        eventTargetList.eq(q3Index++).css('background', 'none');
                        if (q3Index >= eventTargetListLength) {
                            q3Index = 0;
                        }
                        eventTargetList.eq(q3Index).css('background', bgColors[q3BgIndex++]);
                    }
                    q3BgIndex = q3BgIndex < bgColorsLength ? q3BgIndex : 0;
                    break;
                default:
                    break;
            }
        }
    });
});
