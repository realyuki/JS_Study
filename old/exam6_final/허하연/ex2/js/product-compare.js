$(function(){
	var obj = {
		init : function(){
			this.initElements();
			this.initArry();
			this.initHeight();
			this.setLayout();
			this.setEvent();
		},
		initElements : function(){
			this.section = $('.product-compare');
			this.jswrap = this.section.find('.hwan_e_nuhun_jswrap');
			this.jswrapHeight, this.timer;
			this.array = [];
		},
		initArry : function(){
			this.jswrap.removeAttr('style');
			this.array = [];
			$.each(this.jswrap,  $.proxy( function(i){ 
				this.array[i] = this.jswrap.eq(i).height();
			}, this) )
			//console.log(this.array)
		},
		initHeight : function(){
			this.jswrapHeight = Math.max.apply( null , this.array );
		},
		setLayout : function(){
			this.jswrap.outerHeight(this.jswrapHeight);
		},
		setEvent : function(){
			$( window ).resize( $.proxy( this.resize, this) );
		},
		resize : function(){
			this.initArry();
			this.initHeight();
			this.setLayout();
		}
	};
	obj.init();
});