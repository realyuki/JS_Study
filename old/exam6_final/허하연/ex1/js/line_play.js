$(function(){
    var obj = {
        init : function(){
            this.initElements(); 
            this.initDisplay();
            this.setOldindex();
            this.initClickEvent();
        },
        initElements : function(){
            this.wrap   = $('.create_wrap');
            this.button = this.wrap.find('button');
            this.swiperContainer = this.wrap.find('.swiper-container');
            this.imgList = this.swiperContainer.find('li.swiper-slide');
            this.oldValue, this.array = [] ;
            
            this.listSize = this.imgList.length;
            for(var i=0; i < this.listSize; i++){
                this.array[i] = i;
            }

            this.listIndex = Math.floor( Math.random() * this.listSize );
            
        },
        initDisplay : function(){
            $( this.imgList.not(':eq('+this.array[this.listIndex]+')') ).hide();
        },
        initClickEvent : function(){
            this.button.on('click', $.proxy(this.action, this));
        },
        initArray : function(){
            this.listSize = this.imgList.length;
            for(var i=0; i < this.listSize; i++){
                this.array[i] = i;
            }
            //console.log('----------------------------------');
        },
        setIndex : function(){
            this.listSize = this.array.length;
            this.listIndex = Math.floor( Math.random() * this.listSize );
            
            if( this.oldValue !== undefined && this.array[ this.listIndex ] == this.oldValue ) return this.setIndex();
            //console.log( '늘 새로워 > ' +   this.array[ this.listIndex ] );
            
            return true;       
        },
        setOldindex : function(){
            //console.log('old Index > ' + this.array[this.listIndex])
            this.oldValue = this.array[ this.listIndex ];
        },
        action : function(){
            if(this.listSize == 1) this.initArray();
             this.setIndex();
             this.displayImg();
             this.setOldindex();
             this.array.splice(this.listIndex, 1);
        },
        displayImg : function(){
            this.imgList.eq(this.oldValue).fadeOut('slow');
            this.imgList.eq(  this.array[this.listIndex] ).fadeIn('slow');
        }
    };
    obj.init();
});