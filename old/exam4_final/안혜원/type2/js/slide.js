$(function(){
	var obj = {
		init : function(){
			this.setOpt();
			this.setElements();
			this.initLayout();
			this.bindEventHandlers();
		},
		setOpt : function(){
			this.oldIndexNum = this.indexNum = 0;
		},
		setElements : function(){
			this.slideArea = $('.slide_view');
			this.dotArea = $('.slide_dots');
			this.slideWrap = this.slideArea.find('ul');
			this.slideList = this.slideWrap.find('li');
			this.slideBtn = this.slideArea.find('button');
			this.prevBtn = this.slideArea.find('button.btn_prev');
			this.nextBtn = this.slideArea.find('button.btn_next');
			this.dotList = this.dotArea.find('li');
			this.dotBtn = this.dotArea.find('button');
		},
		initLayout : function(){
			this.slideList.hide().eq(this.indexNum).show();
			this.sliderActive();
		},
		bindEventHandlers : function(){
			this.prevBtn.on('click', $.proxy(this.prevAction, this));
			this.nextBtn.on('click', $.proxy(this.nextAction, this));
			this.dotBtn.on('mouseenter', $.proxy(this.dots, this));
		},
		prevAction : function(){
			this.indexNum--;
			if(this.indexNum < 0){
				this.indexNum = this.slideList.length - 1;
			}
			this.slider();
		},
		nextAction : function(){
			this.indexNum++;
			if(this.indexNum > this.slideList.length - 1){
				this.indexNum = 0;
			}
			this.slider();
		},
		slider : function(){
			this.resetSlider();
			this.sliderActive();
			this.oldIndexNum = this.indexNum;
		},
		dots : function(e){
			this.indexNum = $(e.currentTarget).parent().index();
			if(this.oldIndexNum == this.indexNum) return;
			$(e.currentTarget).parent().addClass('active');
			this.slider();
		},
		resetSlider : function(){
			this.dotList.removeClass('active');
			this.slideList.fadeOut().removeClass('active');
		},
		sliderActive : function(){
			this.slideList.eq(this.indexNum).fadeIn().addClass('active');
			this.dotList.eq(this.indexNum).fadeIn().addClass('active');
		}
	};
	obj.init();
});