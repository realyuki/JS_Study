$(function(){
	var obj = {
		init : function(){
			this.setOpt();
			this.setElements();
			this.initLayout();
			this.bindEventHandlers();
		},
		setOpt : function(){
			this.oldIndexNum = this.indexNum = 0;
		},
		setElements : function(){
			this.slideArea = $('.slides_area');
			this.slideWrap = this.slideArea.find('ul');
			this.slideList = this.slideWrap.find('li');
			this.slideItem = this.slideList.find('.img');
			this.slideBtn = this.slideList.find('button');
			this.prevBtn = this.slideArea.find('button.prev');
			this.nextBtn = this.slideArea.find('button.next');
			this.dotBtn = this.slideList.find('button.page');
		},
		initLayout : function(){
			this.slideBtn.css('background', '');
			this.slideItem.hide().eq(this.indexNum).show();
			this.slideItem.eq(this.indexNum).prev().css('background', '#000');
		},
		bindEventHandlers : function(){
			this.prevBtn.on('click', $.proxy(this.prevAction, this));
			this.nextBtn.on('click', $.proxy(this.nextAction, this));
			this.dotBtn.on('mouseenter', $.proxy(this.dots, this));
		},
		prevAction : function(){
			this.indexNum--;
			if(this.indexNum < 0){
				this.indexNum = this.slideList.length - 1;
			}
			this.slider();
		},
		nextAction : function(){
			this.indexNum++;
			if(this.indexNum > this.slideList.length - 1){
				this.indexNum = 0;
			}
			this.slider();
		},
		slider : function(){
			this.slideItem.fadeOut().prev().css('background', '');
			this.slideItem.eq(this.indexNum).fadeIn();
			this.slideItem.eq(this.indexNum).prev().css('background', '#000');
			this.oldIndexNum = this.indexNum;
		},
		dots : function(e){
			this.indexNum = $(e.currentTarget).parent().index();
			if(this.oldIndexNum == this.indexNum) return;
			this.slideBtn.css('background', '').next().fadeOut();
			$(e.currentTarget).css('background', '#000').next().fadeIn();
			this.oldIndexNum = this.indexNum;
		}
	};
	obj.init();
});