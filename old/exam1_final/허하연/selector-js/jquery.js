$(function(){
	var query = [' *', ' span', ' > em > span', ':parent > span', ' > div + p', ' > div ~ span', ' .active', '  #active' ];

	// exam_wrap 갯수만큼 반복
	$('.exam_wrap').each(function(i) {
		var examObj = $(this);
		examObj.find('button').on('click', function(){  
			examObj.find('.exam_q'+query[i]).css('background','yellow');
		
			/* exam1 >> 모든 엘리먼트와 일치하게 하시오. */
			//examObj.find('.exam_q *').css('background','yellow');
			
			/* exam2 >> 태그명이 span 인 엘리먼트와 일치하게 하시오. */
			//examObj.find('.exam_q span').css('background','yellow');

			/* exam3 >> em의 자손이면서 태그명이 span 인 엘리먼트와 일치하게 하시오. */
			//examObj.find('.exam_q > em > span').css('background','yellow');

			/* exam4 >> 부모의 바로 아래 자식이면서 태그명이 span 인 엘리먼트와 일치하게 하시오. */
			//examObj.find('.exam_q:parent > span').css('background','yellow');

			/* exam5 >> div의 형제 엘리먼트로 바로 다음에 나오는 p와 일치하게 하시오. */
			//examObj.find('.exam_q > div + p').css('background','yellow');

			/* exam6 >> div의 형제 엘리먼트로 다음에 나오는 모든 엘리먼트 span과 일치하게 하시오. */
			//examObj.find('.exam_q > div ~ span').css('background','yellow');

			/* exam7 >>클래스명 active를 가지는 모든 엘리먼트와 일치하게 하시오.  */
			//examObj.find('.exam_q .active').css('background','yellow');

			/* exam8 >> 아이디가 active인 엘리먼트와 일치하게 하시오. */
			//examObj.find('.exam_q  #active').css('background','yellow');
		});
	});
});