// javascript code
window.onload = function(){
    // exam_wrap 전체를 받아 온다.
    var examObj = document.getElementsByClassName('exam_wrap');

    // exam_wrap 길이 만큼 반복 
    for(var i=0; i < examObj.length; i++){

        // 각 exam_wrap 안에 있는 button을 찾아 button에 넣는다.
        var button = examObj[i].getElementsByTagName('button')[0];
        // 몇번째 exam_wrap의 버튼인지 카운트하여 button.index 에 저장 
        button.index = i+1;

        // 각 버튼이 클릭되면
        button.onclick = function(){
            // 버튼에 저장 된 index 번호로 각 문제에 맞는 함수명을  funcName에 넣은 후 
            var funcName =  "exam" + this.index;

            // 문제 확인
            //console.log(this.index + ' >> ' + this.parentElement.getElementsByTagName('h1')[0].textContent);

            // 함수 실행
            window[funcName](this.parentElement.getElementsByClassName('exam_q')[0]);
            /*
            if(this.previousSibling.nodeType == 3){
                window[funcName](this.previousSibling.previousSibling);
            }else{
                window[funcName](this.previousSibling);
            }
            */
        };
    }
}

/* exam1 >> 모든 엘리먼트와 일치하게 하시오. */
function exam1(examEl){
    var examChild = examEl.children;
    for(var i = 0; i < examChild.length; i++){
        examChild[i].style.backgroundColor = 'yellow';
    }
}

/* exam2 >> 태그명이 span 인 엘리먼트와 일치하게 하시오. */
function exam2(examEl){
    var examChild = examEl.getElementsByTagName('span');
    for(var i = 0; i < examChild.length; i++){
        examChild[i].style.backgroundColor = 'yellow';
    }
}

/* exam3 >> em의 자손이면서 태그명이 span 인 엘리먼트와 일치하게 하시오. */
function exam3(examEl){
    var examChild = examEl.getElementsByTagName('span');
    for(var i = 0; i < examChild.length; i++){
        if(examChild[i].parentElement.tagName === 'EM'){
            examChild[i].style.backgroundColor = 'yellow';
        }
    }
}

/* exam4 >> 부모의 바로 아래 자식이면서 태그명이 span 인 엘리먼트와 일치하게 하시오. */
function exam4(examEl){
    var examChild = examEl.children;
    for(var i = 0; i < examChild.length; i++){
        if(examChild[i].tagName === 'SPAN'){
            examChild[i].style.backgroundColor = 'yellow';
        }
    }
}

/* exam5 >> div의 형제 엘리먼트로 바로 다음에 나오는 p와 일치하게 하시오. */
function exam5(examEl){
    var examChild = examEl.children;
    for(var i = 0; i < examChild.length; i++){
        if( i > 0 && examChild[i-1].tagName === 'DIV' && examChild[i].tagName === 'P'){
            examChild[i].style.backgroundColor = 'yellow';
        }
    }
    /*
    var examChild = examEl.firstChild.nodeType ===1 ? examEl.firstChild : examEl.firstChild.nextSibling;
    while (examChild) {
        var prevTagName = examChild.tagName;
        examChild = examChild.nextSibling.nodeType ===1 ? examChild.nextSibling : examChild.nextSibling.nextSibling;
        if(examChild){
            if(prevTagName === 'DIV' && examChild.tagName === 'P'){
                examChild.style.backgroundColor = 'yellow';
            }
        }
      }
    */
}

/* exam6 >> div의 형제 엘리먼트로 다음에 나오는 모든 엘리먼트 span과 일치하게 하시오. */
function exam6(examEl){
    var examChild = examEl.getElementsByTagName('div')[0];
    while(examChild = examChild.nextSibling){
        if(1===examChild.nodeType){
            examChild.style.backgroundColor = 'yellow';
        }
    }
}

/* exam7 >>클래스명 active를 가지는 모든 엘리먼트와 일치하게 하시오.  */
function exam7(examEl){
    var examChild = examEl.getElementsByClassName('active');
    for(var i = 0; i < examChild.length; i++){
        examChild[i].style.backgroundColor = 'yellow';
    }
}

/* exam8 >> 아이디가 active인 엘리먼트와 일치하게 하시오. */
function exam8(examEl){
    document.getElementById('active').style.backgroundColor = 'yellow';
}