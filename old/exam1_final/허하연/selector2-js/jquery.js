$(function(){
	var query = [' *:first', ' *:last', ' > :first-child', ' > :last-child', ' > :nth-child(2)', ' > :eq(2)', ' > :gt(2)', ' > :lt(2)' ];

	// exam_wrap 갯수만큼 반복
	$('.exam_wrap').each(function(i) {
		var examObj = $(this);
		examObj.find('button').on('click', function(){  
			examObj.find('.exam_q'+query[i]).css('background','yellow');

			/* exam1 >> 처음으로 일치하는 엘리먼트를 반환하시오. */
			//examObj.find('.exam_q *:first').css('background','yellow');
			
			/* exam2 >> 마지막으로 일치하는 엘리먼트를 반환하시오. */
			//examObj.find('.exam_q *:last').css('background','yellow');

			/* exam3 >> 첫번째 자식 엘리먼트를 반환하시오. */
			//examObj.find('.exam_q > :first-child').css('background','yellow');

			/* exam4 >> 마지막 자식 엘리먼트를 반환하시오. */
			//examObj.find('.exam_q > :last-child').css('background','yellow');

			/* exam5 >> 2번째 자식 엘리먼트를 반환하시오. */
			//examObj.find('.exam_q > :nth-child(2)').css('background','yellow');

			/* exam6 >> 3번째로 일치하는 엘리먼트를 반환하시오. */
			//examObj.find('.exam_q > :eq(2)').css('background','yellow');

			/* exam7 >> 3번째 엘리먼트 이후의 엘리먼트를 반환하시오.  */
			//examObj.find('.exam_q > :gt(2)').css('background','yellow');

			/* exam8 >> 3번째 엘리먼트 이전의 엘리먼트를 반환하시오. */
			//examObj.find('.exam_q > :lt(2)').css('background','yellow');
		});
	});
});