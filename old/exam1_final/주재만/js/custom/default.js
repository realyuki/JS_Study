$(function(){
	'use strict';
	$(".show-hide-button").on("click", function(){
		var text = $(this).text();
		if ( $(this).siblings( $(this).data("target") ).hasClass("hide") ) {
			$(this).siblings( $(this).data("target") ).removeClass("hide");
			text = text.split("펼치기").join("접기");
		}
		else {
			$(this).siblings( $(this).data("target") ).addClass("hide");
			text = text.split("접기").join("펼치기");
		}
		$(this).text( text );
	});
});
