window.onload = function() {
    // var examObj1 = document.getElementsByClassName('exam_wrap')[0];
    // examObj1.getElementsByTagName('button')[0].onclick = function() {
    //     var examChild = examObj1.getElementsByClassName('exam_q')[0].children;
    //     for (var i = 0; i < examChild.length; i++) {
    //         examChild[i].style.backgroundColor = 'yellow';
    //     }
    // };
    var buttonArray = document.getElementsByTagName('button');
    buttonArray[0].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	answerNode.getElementsByTagName('div')[0].style.backgroundColor = 'yellow';
    }
    buttonArray[1].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	var answerNodeChildrenLength = answerNode.getElementsByTagName('div').length;
    	answerNode.getElementsByTagName('div')[answerNodeChildrenLength-1].style.backgroundColor = 'yellow';
    }
    buttonArray[2].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	answerNode.firstChild.style.backgroundColor = 'yellow';
    }
    buttonArray[3].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	answerNode.lastChild.style.backgroundColor = 'yellow';
    }
    buttonArray[4].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	answerNode.childNodes[1].style.backgroundColor = 'yellow';
    }
    buttonArray[5].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	answerNode.getElementsByTagName('div')[2].style.backgroundColor = 'yellow';
    }
    buttonArray[6].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	var answerNodeChildrenLength = answerNode.getElementsByTagName('div').length;
    	for ( var i = 3; i < answerNodeChildrenLength; i++ ) {
    		answerNode.getElementsByTagName('div')[i].style.backgroundColor = 'yellow';
    	}
    }
    buttonArray[7].onclick = function() {
    	var answerNode = this.parentElement.getElementsByClassName('exam_q')[0];
    	for ( var i = 0; i < 2; i++ ) {
    		answerNode.getElementsByTagName('div')[i].style.backgroundColor = 'yellow';
    	}
    }
}
