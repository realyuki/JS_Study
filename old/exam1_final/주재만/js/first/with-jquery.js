$(function() {
    $('.exam_wrap > button').on('click', function() {
        var selectArea = $(this).parent();
        var selectAreaIndex = 0;
        if (selectArea != null) {
            selectAreaIndex = selectArea.index();
        }

        switch (selectAreaIndex) {
            case 0:
                selectArea.find('.exam_q *').css('background', 'yellow');
                break;
            case 1:
                selectArea.find('.exam_q span').css('background', 'yellow');
                break;
            case 2:
                selectArea.find('.exam_q em').find('span').css('background', 'yellow');
                break;
            case 3:
                selectArea.find('.exam_q > span').css('background', 'yellow');
                break;
            case 4:
                selectArea.find('.exam_q div').siblings('p').first().css('background', 'yellow');
                break;
            case 5:
                selectArea.find('.exam_q div').nextAll('span').css('background', 'yellow');
                break;
            case 6:
                selectArea.find('.exam_q .active').css('background', 'yellow');
                break;
            case 7:
                selectArea.find('.exam_q #active').css('background', 'yellow');
                break;
            default:
                break;
        }
    });
});
