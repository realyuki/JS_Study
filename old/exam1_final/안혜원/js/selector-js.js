var doc = document;

window.onload = function(){
    //Q. 모든 엘리먼트와 일치하게 하시오.
    var examObj1 = doc.getElementsByClassName('exam_wrap')[0];
    examObj1.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj1.getElementsByClassName('exam_q')[0].children;
        for(var i = 0; i < examChild.length; i++){
            examChild[i].style.backgroundColor = 'yellow';
        }
    };
 
    //Q. 태그명이 span 인 엘리먼트와 일치하게 하시오.
    var examObj2 = doc.getElementsByClassName('exam_wrap')[1];
    examObj2.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj2.getElementsByTagName('span');
        for(var i = 0; i < examChild.length; i++){
            examChild[i].style.backgroundColor = 'yellow';
        }
    };
 
    //Q. em의 자손이면서 태그명이 span 인 엘리먼트와 일치하게 하시오.
    var examObj3 = doc.getElementsByClassName('exam_wrap')[2];
    examObj3.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj3.getElementsByTagName('em');
        for(var i = 0; i < examChild.length; i++){
            var aa = examChild[i].querySelector('span');
            aa.style.backgroundColor = 'yellow';
        }
    };
 
    //Q. 부모의 바로 아래 자식이면서 태그명이 span 인 엘리먼트와 일치하게 하시오.
    var examObj4 = doc.getElementsByClassName('exam_wrap')[3];
    examObj4.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj4.getElementsByTagName('span');
        for(var i = 0; i < examChild.length; i++){
            examChild[i].style.backgroundColor = 'yellow';
        }
    };
 
    //Q. div의 형제 엘리먼트로 바로 다음에 나오는 p와 일치하게 하시오.
    var examObj5 = doc.getElementsByClassName('exam_wrap')[4];
    examObj5.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj5.getElementsByClassName('exam_q')[0];
        var aa = examChild.querySelector('div').nextElementSibling;
        var bb = examObj5.querySelector('p');
        if(aa === bb) {
            aa.style.backgroundColor = 'yellow';
        }
    };
 
    //Q. div의 형제 엘리먼트로 다음에 나오는 모든 엘리먼트 span과 일치하게 하시오.
    var examObj6 = doc.getElementsByClassName('exam_wrap')[5];
    examObj6.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj6.getElementsByClassName('exam_q')[0];
        var aa = examChild.querySelector('div').nextElementSibling.nextElementSibling;
        var bb = examObj6.querySelector('span');
        if(aa === bb) {
            aa.style.backgroundColor = 'yellow';
        }
    };
 
    //Q. 클래스명 active를 가지는 모든 엘리먼트와 일치하게 하시오.
    var examObj7 = doc.getElementsByClassName('exam_wrap')[6];
    examObj7.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj7.getElementsByClassName('active');
        for(var i = 0; i < examChild.length; i++){
            examChild[i].style.backgroundColor = 'yellow';
        }
    };
 
    //Q. 아이디가 active인 엘리먼트와 일치하게 하시오.
    var examObj8 = doc.getElementsByClassName('exam_wrap')[7];
    examObj8.getElementsByTagName('button')[0].onclick = function(){
    	var aa = examObj8.querySelector('#active');
    	aa.style.backgroundColor = 'yellow';
    };
}