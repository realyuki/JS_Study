$(function(){
	//Q. 태그명이 span 인 엘리먼트와 일치하게 하시오.
	$('.exam_wrap:eq(1)').find('button').on('click', function(){
		$('.exam_wrap:eq(1)').find('span').css('background-color', 'yellow');
	})

	//Q. em의 자손이면서 태그명이 span 인 엘리먼트와 일치하게 하시오.
	$('.exam_wrap:eq(2)').find('button').on('click', function(){
		$('.exam_wrap:eq(2)').find('em span').css('background-color', 'yellow');
	})

	//Q. 부모의 바로 아래 자식이면서 태그명이 span 인 엘리먼트와 일치하게 하시오.
	$('.exam_wrap:eq(3)').find('button').on('click', function(){
		$('.exam_wrap:eq(3)').find('div > span').css('background-color', 'yellow');
		$('.exam_wrap:eq(3)').find('em > span').css('background-color', 'yellow');
	})

	//Q.div의 형제 엘리먼트로 바로 다음에 나오는 p와 일치하게 하시오.
	$('.exam_wrap:eq(4)').find('button').on('click', function(){
		$('.exam_wrap:eq(4)').find('div + p').css('background-color', 'yellow');
	})

	//Q. div의 형제 엘리먼트로 다음에 나오는 모든 엘리먼트 span과 일치하게 하시오.
	$('.exam_wrap:eq(5)').find('button').on('click', function(){
		$('.exam_wrap:eq(5)').find('.exam_q > div').nextAll('span').css('background-color', 'yellow');
	})

	//Q. 클래스명 active를 가지는 모든 엘리먼트와 일치하게 하시오.
	$('.exam_wrap:eq(6)').find('button').on('click', function(){
		$('.exam_wrap:eq(6)').find('.active').css('background-color', 'yellow');
	})

	//Q. 아이디가 active인 엘리먼트와 일치하게 하시오.
	$('.exam_wrap:eq(7)').find('button').on('click', function(){
		$('.exam_wrap:eq(7)').find('#active').css('background-color', 'yellow');
	})
});


