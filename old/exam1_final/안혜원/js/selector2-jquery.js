$(function(){
	//Q. 처음으로 일치하는 엘리먼트를 반환하시오.
	$('.exam_wrap:eq(0)').find('button').on('click', function(){
		$('.exam_wrap:eq(0)').find('.exam_q div:first').css('background-color', 'yellow');
	})

	//Q. 마지막으로 일치하는 엘리먼트를 반환하시오.
	$('.exam_wrap:eq(1)').find('button').on('click', function(){
		$('.exam_wrap:eq(1)').find('.exam_q div:last').css('background-color', 'yellow');
	})

	//Q. 첫번째 자식 엘리먼트를 반환하시오.
	$('.exam_wrap:eq(2)').find('button').on('click', function(){
		$('.exam_wrap:eq(2)').find('.exam_q div:first-child em').css('background-color', 'yellow');
	})

	//Q. 마지막 자식 엘리먼트를 반환하시오.
	$('.exam_wrap:eq(3)').find('button').on('click', function(){
		$('.exam_wrap:eq(3)').find('.exam_q div:last-child em').css('background-color', 'yellow');
	})

	//Q. 2번째 자식 엘리먼트를 반환하시오.
	$('.exam_wrap:eq(4)').find('button').on('click', function(){
		$('.exam_wrap:eq(4)').find('.exam_q div:nth-child(2) div').css('background-color', 'yellow');
	})

	//Q. 3번째로 일치하는 엘리먼트를 반환하시오.
	$('.exam_wrap:eq(5)').find('button').on('click', function(){
		$('.exam_wrap:eq(5)').find('.exam_q div:eq(2)').css('background-color', 'yellow');
	})

	//Q. 3번째 엘리먼트 이후의 엘리먼트를 반환하시오.
	$('.exam_wrap:eq(6)').find('button').on('click', function(){
		$('.exam_wrap:eq(6)').find('.exam_q div:eq(2)').nextAll().css('background-color', 'yellow');
	})

	$('.exam_wrap:eq(7)').find('button').on('click', function(){
		$('.exam_wrap:eq(7)').find('.exam_q div:eq(2)').prevAll().css('background-color', 'yellow');
	})
});