window.onload = function(){
    // Exam #1
    var examObj1 = document.getElementsByClassName('exam_wrap')[0];
    examObj1.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj1.getElementsByClassName('exam_q')[0].children,
        examFirst = examChild[0];
        examFirst.style.backgroundColor = 'yellow';
    };

    // Exam #2
    var examObj2 = document.getElementsByClassName('exam_wrap')[1];
    examObj2.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj2.getElementsByClassName('exam_q')[0].children,
        examLast = examChild.lastElementChild;
        for(var i = 0; i < examChild.length; i++){
            var examChildLast = examChild[i].lastElementChild;
            if ( i == examChild.length - 1) {
                examChildLast.style.backgroundColor = 'yellow';
            }
        }
    };

    // Exam #3
    var examObj3 = document.getElementsByClassName('exam_wrap')[2];
    examObj3.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj3.getElementsByClassName('exam_q')[0],
        examFirst = examChild.querySelectorAll(':first-child');
        for(var i = 0; i < examFirst.length; i++){
            examFirst[i].style.backgroundColor = 'yellow';
        };
    };

    // Exam #4
    var examObj4 = document.getElementsByClassName('exam_wrap')[3];
    examObj4.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj4.getElementsByClassName('exam_q')[0],
        examLast = examChild.querySelectorAll(':last-child');
        for(var i = 0; i < examLast.length; i++){
            examLast[i].style.backgroundColor = 'yellow';
        };
    };

    // Exam #5
    var examObj5 = document.getElementsByClassName('exam_wrap')[4];
    examObj5.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj5.getElementsByClassName('exam_q')[0].children,
        examSecond = examChild[1];
        examSecond.style.backgroundColor = 'yellow';
        for(var i = 0; i < examChild.length; i++){
            if (examChild[i].hasChildNodes()) {
                var examChild2 = examChild[i].children,
                examSecond2 = examChild2[1];
                if ( examSecond2 !== window.undefined) {
                    examSecond2.style.backgroundColor = 'yellow';
                }
            }
        }
    };

    // Exam #6
    var examObj6 = document.getElementsByClassName('exam_wrap')[5];
    examObj6.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj6.getElementsByClassName('exam_q')[0].children;
        examChild[2].style.backgroundColor = 'yellow';
    };

    // Exam #7
    var examObj7 = document.getElementsByClassName('exam_wrap')[6];
    examObj7.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj7.getElementsByClassName('exam_q')[0].children;
        for(var i = 0; i < examChild.length; i++){
            if ( i > 2 ) {
                examChild[i].style.backgroundColor = 'yellow';
            }
        }
    };

    // Exam #8
    var examObj8 = document.getElementsByClassName('exam_wrap')[7];
    examObj8.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj8.getElementsByClassName('exam_q')[0].children;
        for(var i = 0; i < examChild.length; i++){
            if ( i < 2 ) {
                examChild[i].style.backgroundColor = 'yellow';
            }
        }
    };
}