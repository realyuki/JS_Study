$(function(){
    // Exam #1
    var examObj1 = $('.exam_wrap:eq(0)');
    examObj1.find('button').on('click', function(){
        examObj1.find('.exam_q :first').css('background','yellow');
    });

    // Exam #2
    var examObj2 = $('.exam_wrap:eq(1)');
    examObj2.find('button').on('click', function(){
        examObj2.find('.exam_q :last').css('background','yellow');
    });


    // Exam #3
    var examObj3 = $('.exam_wrap:eq(2)');
    examObj3.find('button').on('click', function(){
        examObj3.find('.exam_q :first-child').css('background','yellow');
    });


    // Exam #4
    var examObj4 = $('.exam_wrap:eq(3)');
    examObj4.find('button').on('click', function(){
        examObj4.find('.exam_q :last-child').css('background','yellow');
    });


    // Exam #5
    var examObj5 = $('.exam_wrap:eq(4)');
    examObj5.find('button').on('click', function(){
        examObj5.find('.exam_q :nth-child(2)').css('background','yellow');
    });


    // Exam #6
    var examObj6 = $('.exam_wrap:eq(5)');
    examObj6.find('button').on('click', function(){
        examObj6.find('.exam_q :eq(2)').css('background','yellow');
    });


    // Exam #7
    var examObj7 = $('.exam_wrap:eq(6)');
    examObj7.find('button').on('click', function(){
        examObj7.find('.exam_q :gt(2)').css('background','yellow');
    });


    // Exam #8
    var examObj8 = $('.exam_wrap:eq(7)');
    examObj8.find('button').on('click', function(){
        examObj8.find('.exam_q :lt(2)').css('background','yellow');
    });
});



