window.onload = function(){
    // Exam #1
    var examObj1 = document.getElementsByClassName('exam_wrap')[0];
    examObj1.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj1.getElementsByClassName('exam_q')[0].children;
        for(var i = 0; i < examChild.length; i++){
            examChild[i].style.backgroundColor = 'yellow';
        }
    };

    // Exam #2
    var examObj2 = document.getElementsByClassName('exam_wrap')[1];
    examObj2.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj2.querySelectorAll('.exam_q span');
        for(var i = 0; i < examChild.length; i++) {
            examChild[i].style.backgroundColor = 'yellow';
        }
    };

    // Exam #3
    var examObj3 = document.getElementsByClassName('exam_wrap')[2];
    examObj3.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj3.querySelectorAll('.exam_q em span');
        for(var i = 0; i < examChild.length; i++) {
            examChild[i].style.backgroundColor = 'yellow';
        }
    };

    // Exam #4
    var examObj4 = document.getElementsByClassName('exam_wrap')[3];
    examObj4.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj4.querySelectorAll('.exam_q > span');
        for(var i = 0; i < examChild.length; i++) {
            examChild[i].style.backgroundColor = 'yellow';
        }
    };

    // Exam #5
    var examObj5 = document.getElementsByClassName('exam_wrap')[4];
    examObj5.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj5.querySelectorAll('.exam_q div + p');
        for(var i = 0; i < examChild.length; i++) {
            examChild[i].style.backgroundColor = 'yellow';
        }
    };

    // Exam #6
    var examObj6 = document.getElementsByClassName('exam_wrap')[5];
    examObj6.getElementsByTagName('button')[0].onclick = function(){
        var examChild = examObj6.querySelectorAll('.exam_q div ~ span');
        for(var i = 0; i < examChild.length; i++) {
            examChild[i].style.backgroundColor = 'yellow';
        }
    };

    // Exam #7
    var examObj7 = document.getElementsByClassName('exam_wrap')[6];
    examObj7.getElementsByTagName('button')[0].onclick = function(){
        var examChildActive = examObj7.getElementsByClassName('active');
        for(var i = 0; i < examChildActive.length; i++) {
            examChildActive[i].style.backgroundColor = 'yellow';
        }
    };

    // Exam #8
    var examObj8 = document.getElementsByClassName('exam_wrap')[7];
    examObj8.getElementsByTagName('button')[0].onclick = function(){
        var examActive = document.getElementById('active');
        examActive.style.backgroundColor = 'yellow';
    };
}