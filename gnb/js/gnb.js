;(function (win, $) {
    'use strict';

    var namespace = win.task || {};
    namespace.ui = namespace.ui || {};

    namespace.ui.config = {
        setMoWidth : 768
    }

    namespace.ui.util = {
        isSupportTransform : function(){
            return('WebkitTransform' in document.body.style || 'MozTransform' in document.body.style || 'msTransform' in document.body.style || 'OTransform' in document.body.style || 'transform' in document.body.style)
        }(),
        windowWidth : function(){
            return(window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth);
        }
    }

    namespace.ui.gnb = (function(){
        var taskFunc = function(container, args){
            var defParams = {
                container : '#gnb',
                menuList : '.menu',
                secondList : '.box',
                openWrap : '.gnb_open',
                closeWrap : '.gnb_close',
                openBtn : '#btn_gnbopen',
                closeBtn : '#btn_gnbclose',
                setLeft : '0'
            };
            var thisObj = this;
            thisObj.init = function(){
                if ((thisObj.container = container).length < 0) return;

                thisObj.opts = $.extend(defParams, (args || {}));
                thisObj.setElements();
                if(namespace.ui.util.isSupportTransform){
                    thisObj.initLayout();
                    thisObj.resizeEvent();
                    thisObj.setEvent();
                }else{
                    this.setPcEvent(true);
                }
            },
            thisObj.setElements = function(){
                thisObj.gnbMo = $(thisObj.opts.container);
                thisObj.gnbLinkPc = $(thisObj.opts.menuList).children();
                thisObj.gnbOpen = $(thisObj.opts.openWrap).find(thisObj.opts.openBtn);
                thisObj.gnbClose = $(thisObj.opts.closeWrap).find(thisObj.opts.closeBtn);
            },
            thisObj.initLayout = function(){
                thisObj.setMode = (namespace.ui.util.windowWidth() <= namespace.ui.config.setMoWidth) ? 'mo' : 'pc';
            },
            thisObj.resizeEvent = function(){
                $(window).on('resize', thisObj.setEvent);
            },
            thisObj.setEvent = function(){
                if(namespace.ui.util.windowWidth() <= namespace.ui.config.setMoWidth){
                    if(thisObj.setMode === 'mo'){
                        thisObj.setMode = 'pc';
                        thisObj.setPcLayout()
                        thisObj.setMoEvent(true);
                        thisObj.setPcEvent(false);
                    }
                }else{
                    if(thisObj.setMode === 'pc'){
                        thisObj.setMode = 'mo';
                        thisObj.setMoLayout();
                        thisObj.setPcEvent(true);
                        thisObj.setMoEvent(false);
                    }
                }
            },
            thisObj.setPcEvent = function(type){
                if(thisObj.gnbMo.is(':visible')){
                    thisObj.setClose();
                }
                if(type){
                    thisObj.gnbLinkPc.on('mouseenter mouseleave focusin',thisObj.setPcHover);
                }else{
                    thisObj.gnbLinkPc.off('mouseenter mouseleave focusin', thisObj.setPcHover);
                }
            },
            thisObj.setMoEvent = function(type){
                if(type){
                    thisObj.gnbOpen.on('click', thisObj.setOpen);
                    thisObj.gnbClose.on('click', thisObj.setClose);
                }else{
                    thisObj.gnbOpen.off('click', thisObj.setOpen);
                    thisObj.gnbClose.off('click', thisObj.setClose);
                }
            },
            thisObj.setPcLayout = function(){
                thisObj.gnbLinkPc.find(thisObj.opts.secondList).css('display', '');
            },
            thisObj.setMoLayout = function(){
                thisObj.gnbLinkPc.find(thisObj.opts.secondList).hide();
            },
            thisObj.setPcHover = function(e){
                var _this = $(e.currentTarget);
                if(e.type == 'mouseenter' || e.type == 'focusin'){
                    if(_this.find(thisObj.opts.secondList).is(':visible')) return;
                    _this.find(thisObj.opts.secondList).show();
                    thisObj.outFocus(_this);
                }else if(e.type == 'mouseleave'){
                    _this.find(thisObj.opts.secondList).hide();
                }
            },
            thisObj.setOpen = function(){
                thisObj.setPcLayout();
                thisObj.gnbMo.css('left', thisObj.opts.setLeft);
            },
            thisObj.setClose = function(){
                thisObj.gnbMo.css('left', '');
            },
            thisObj.outFocus = function(_this){
                _this.on('focusoutside', thisObj.focusoutsideEvent);
            },
            thisObj.focusoutsideEvent = function(e){
                var _this = $(e.currentTarget);
                _this.find(thisObj.opts.secondList).hide();
                _this.off('focusoutside');
            }
            thisObj.init();
        };

        $.fn.taskFunc = function(){
            // for(var i=0, max=this.length; i<max; i++){
            //     new taskFunc(this);
            // }
            this.each(function(){
                new taskFunc(this);
            })
        }

        return {
            init : function(){
                this.obj = $('#header');
                $('.header').taskFunc();
                // if('undefined' === typeof this.taskFunc){
                //  this.taskFunc = [];
                //  for(var i=0, max=this.obj.length; i<max; i++){
                //      this.taskFunc[i] = new taskFunc(this.obj.eq(i));
                //  }
                // }else{
                //  //this.reInit();
                // }
            }
        }
    })();

    $(function(){
        namespace.ui.gnb.init();
    });

})(window, window.jQuery);